# Brightness

A small Rust program that uses ddcutil to control your monitor's brightness.


## Usage

You can specify the brightness values directly as command-line arguments or enter them interactively when prompted.

To run the program, use the following command:

    brightness [OPTIONS] [BRIGHTNESS_VALUES]

#### Options

    -r, --rebuild: Rebuild the configuration file.
    [BRIGHTNESS_VALUES]: Brightness values for each monitor (0-100), separated by spaces, 0 for all monitors.


## Dependencies

This program depends on the following external crates and utilities:

    clap: Command-line argument parsing.
    regex: Regular expression matching.
    ddcutil: A utility for controlling monitor settings via DDC/CI (Display Data Channel/Command Interface).
    lsmod: A utility for listing loaded kernel modules.
    modprobe: A utility for loading and unloading kernel modules.
    pkexec (optional): A program used by polkit for UAC style privilege escalation.


## Configuration File

The program uses a configuration file named `$XDG_CONFIG_HOME/brightness.conf` or `~/.config/brightness.conf` to store monitor IDs. The file is automatically created and updated when necessary. If the file doesn't exist or needs to be rebuilt, the program prompts the user to enter the desired monitor IDs. The format of the configuration file is as follows:

    monitor_ids=([UID1, UID2, ...])

## Example Usage

Here are some example usages of the program:

Set brightness to 50 for all monitors:

    brightness 50

Set brightness to 80 for the first monitor and 70 for the second monitor:

    brightness 80 70

Rebuild the configuration file and interactively enter brightness values:

    brightness -r

    Enter the monitor number(s), separated by spaces, 0 for all displays:
    1 2

    Please enter brightness values (0-100) separated by spaces:
    80 70

## Limitations

- ddcutil, lsmod and modeprobe are required (pkexec optional) to control the monitors brightness. Make sure it's installed and available in your system's PATH.

- `brightness` assumes that the i2c-dev module is required to control the monitor brightness and attempts to load it if necessary.

- `brightness` needs access to `$XDG_CONFIG_HOME/brightness.conf` but will fall back to `~/.config/brightness.conf`.

- All code was written and run on a Arch based system, things should work on other distros but if they don't, feel free to make an issue / contrubition and I'll try to commit.


## License

GNU AGPLv3
