use std::{collections::hash_map::DefaultHasher, hash::Hasher};

#[derive(Clone)]
pub struct Monitor {
    monitor_uid: u8,
    monitor_i2c: u8,

    monitor_drm: String,
    monitor_mfg: String,
    monitor_model: String,
}


impl Monitor {
    pub fn generate(monitor_info: &Vec<String>) -> Monitor {
        let monitor_uid;
        let mut monitor_i2c = None;
        let mut monitor_drm = None;
        let mut monitor_mfg = None;
        let mut monitor_model = None;

        // Concatenate the lines from monitor_info
        let ddcutil_output = monitor_info.join("\n");

        // Calculate monitor_uid using the provided function
        let mut hasher = DefaultHasher::new();
        for line in ddcutil_output.lines() {
            if let Some(value) = line.trim().split(':').nth(1) {
                hasher.write(value.trim().as_bytes());
            }
        }
        monitor_uid = Some(hasher.finish() as u8);

        for line in monitor_info {
            if line.starts_with("I2C") {
                if let Some(value) = line.splitn(2, '-').last().and_then(|s| s.trim().parse::<u8>().ok()) {
                    monitor_i2c = Some(value);
                }    
            }
            else if line.starts_with("Mfg") {
                monitor_mfg = Some(line.splitn(2, ':').last().unwrap().trim().to_string());
            }
            else if line.starts_with("DRM") {
                monitor_drm = Some(line.splitn(2, ':').last().unwrap().trim().to_string());
            }
            else if line.starts_with("Model") {
                monitor_model = Some(line.splitn(2, ':').last().unwrap().trim().to_string());
            }
            else {

            }
        }
        
        Monitor {
            monitor_uid: monitor_uid.unwrap_or_default(),
            monitor_i2c: monitor_i2c.unwrap_or_default(),
            monitor_drm: monitor_drm.unwrap_or_else(|| String::from("")),
            monitor_mfg: monitor_mfg.unwrap_or_else(|| String::from("")),
            monitor_model: monitor_model.unwrap_or_else(|| String::from("")),
        } 
    }

    pub fn get_monitor_uid(&self) -> u8 {
        self.monitor_uid
    }

    pub fn get_monitor_i2c(&self) -> u8 {
        self.monitor_i2c
    }

    pub fn get_monitor_drm(&self) -> &str {
        self.monitor_drm.as_str()
    }
    
    pub fn get_monitor_mfg(&self) -> &str {
        self.monitor_mfg.as_str()
    }

    pub fn get_monitor_model(&self) -> &str {
        self.monitor_model.as_str()
    }

    pub fn to_string(&self) {
        println!("UID: {}", self.get_monitor_uid());
        println!("Bus: /dev/i2c-{}", self.get_monitor_i2c());
        println!("Connector: {}", self.get_monitor_drm());
        println!("Manufacture: {}", self.get_monitor_mfg());
        println!("Model: {}", self.get_monitor_model());
    }
}