use clap::{App, Arg};
use monitor::Monitor;
use regex::Regex;
use std::env;
use std::fs::{self, OpenOptions, File};
use std::io::{Write, BufRead, Error, self};
use std::ops::ControlFlow;
use std::path::{Path, PathBuf};
use std::process::{Command, ExitStatus};

mod monitor;

static BRIGHTNESS_VER: &str = "24.11.0";

fn get_monitors() -> Vec<monitor::Monitor> {
    // Run the 'ddcutil detect' command and capture the output
    let output = Command::new("ddcutil")
        .arg("detect")
        .output()
        .expect("Failed to execute ddcutil");

    // Convert the output to lines
    let binding = String::from_utf8_lossy(&output.stdout);
    let ddcutil_output = binding.lines();

    let mut monitor_vec: Vec<monitor::Monitor> = Vec::new();
    let mut monitor_info: Vec<String> = Vec::new();
    let end_marker = "VCP";

    for line in ddcutil_output {
        let line_without_spaces = line.trim().replace(char::is_whitespace, "");

        if !line_without_spaces.is_empty() {
            monitor_info.push(line_without_spaces.to_string());
        }

        if line_without_spaces.starts_with(end_marker) {
            let monitor = monitor::Monitor::generate(&monitor_info);
            monitor_vec.push(monitor);
            monitor_info.clear();        
        }
    }
    
    monitor_vec
}

fn has_root_privileges() -> bool {
    match Command::new("id").arg("-u").output() {
        Ok(output) => {
            if let Ok(uid_str) = String::from_utf8(output.stdout) {
                if let Ok(uid) = uid_str.trim().parse::<u32>() {
                    return uid == 0; // Check if the EUID is 0 (root).
                }
            }
        }
        Err(_) => {}
    }
    false
}

fn run_pkexec_command<S>(command: S) -> Result<ExitStatus, Error>
where
    S: AsRef<str>,
{
    if has_root_privileges() {
        // The current user is root or has sudo privileges, so we won't use pkexec.
        let output = Command::new("sh")
            .arg("-c")
            .arg(command.as_ref())
            .output()?;
        
        Ok(output.status)
    } else {
        // The current user is not root or doesn't have sudo privileges, use pkexec.
        let output = Command::new("pkexec")
            .arg("sh")
            .arg("-c")
            .arg(command.as_ref())
            .output();

        match output {
            Ok(output) => {
                Ok(output.status)
            }
            Err(error) => {
                if error.kind() == std::io::ErrorKind::NotFound {
                    // pkexec is not found, inform the user to install Polkit or run as root.
                    writeln!(
                        std::io::stderr(),
                        "Error: pkexec not found. Please install Polkit or run this command as root."
                    )
                    .ok(); // Ignore write errors
                }
                Err(error)
            }
        }
    }
}

fn load_i2cdev() {
    let command = "modprobe --quiet i2c-dev";
    
    match run_pkexec_command(command) {
        Ok(status) if status.success() => {
            println!("i2c-dev module loaded successfully");
        },
        Ok(_) => {
            println!("Failed to load i2c-dev module");
        },
        Err(err) => {
            println!("Error loading i2c-dev module: {}", err);
        }
    }
}

fn is_i2cdev_loaded() -> bool {
    let output = Command::new("lsmod")
        .output()
        .expect("Failed to execute modprobe");

    let lsmod_output = String::from_utf8_lossy(&output.stdout);

    lsmod_output.lines().any(|line| line.contains("i2c_dev"))
}

fn is_valid_integers(val: String) -> Result<(), String> {
    match val.parse::<u8>() {
        Ok(num) => {
            if num <= 100 {
                Ok(())
            } else {
                Err(String::from("Brightness value must be between 0 and 100"))
            }
        }
        Err(_) => Err(String::from("Invalid brightness value")),
    }
}

fn get_valid_integers() -> Vec<u8> {
    loop {
        let stdin = io::stdin();
        let input = stdin.lock().lines().next().unwrap().unwrap();
        let input_values: Vec<u8> = input
            .split_whitespace()
            .map(|v| v.parse().unwrap())
            .collect();

        let validated_values: Vec<u8> = input_values
            .into_iter()
            .filter(|&v| is_valid_integers(v.to_string()).is_ok())
            .collect();

        if !validated_values.is_empty() {
            return validated_values;
        }
    }
}

fn input_monitor_ids() -> Vec<u8> {
    let mut monitor_ids = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    if handle.read_line(&mut monitor_ids).is_err() {
        println!("Couldn't read input.")
    };

    let ids: Vec<u8> = monitor_ids
        .split_whitespace()
        .filter_map(|s| s.parse().ok())
        .collect();

    ids
}

fn get_xdg_config() -> PathBuf {
    // Determine XDG_CONFIG_HOME or fallback to a default directory
    let xdg_config_home = match env::var("XDG_CONFIG_HOME") {
        Ok(val) => val,
        Err(_) => {
            // If XDG_CONFIG_HOME is not set, use the default home directory
            match env::var("HOME") {
                Ok(home) => format!("{}/.config", home),
                Err(_) => {
                    // Fallback to the current directory if HOME is not set
                    // This is not recommended for production code but is used for simplicity
                    ".".to_string()
                }
            }
        }
    };

    // Construct the full path for the brightness.conf file in XDG_CONFIG_HOME
    Path::new(&xdg_config_home).join("brightness.conf").into()
}

fn write_config_file(monitor_ids: &[u8]) -> io::Result<()> {
    let config_path = get_xdg_config();

    // Open or create the file for writing
    let mut file = OpenOptions::new()
        .create(true)
        .write(true)
        .open(&config_path)?;

    // Write the configuration data
    writeln!(file, "brightness_ver={}", BRIGHTNESS_VER)?;
    writeln!(file, "monitor_ids=({:?})", monitor_ids)?;

    Ok(())
}

fn read_config_file() -> Vec<u8> {
    let mut ver: String = "null".to_string();
    let mut monitor_ids: Vec<u8> = Vec::new();

    let config_path = get_xdg_config();

    if let Ok(file) = File::open(&config_path) {
        let reader = io::BufReader::new(file);

        for line in reader.lines() {
            if let Ok(line) = line {
                if let Some(captures) = Regex::new(r"brightness_ver=(\d+\.\d+\.\d+)").unwrap().captures(&line) {
                    ver = captures[1].to_string(); // Assign to the outer ver variable
                }

                if let Some(captures) = Regex::new(r"monitor_ids=\(\[(.*?)\]\)").unwrap().captures(&line) {
                    let ids = captures.get(1).unwrap().as_str();

                    // Check if ids is not empty before attempting to parse
                    if !ids.trim().is_empty() {
                        monitor_ids = ids.split(',').map(|id| id.trim().parse::<u8>()).collect::<Result<Vec<u8>, _>>().unwrap();
                    } else {
                        // Set monitor_ids to a new vector with a single 0
                        monitor_ids = vec![0];
                    }
                    break;
                }
            }
        }
    }

    if ver != BRIGHTNESS_VER {
        println!("Brightness version in the config file ({}) does not match the expected version ({}). Please consider rebuilding the file with the -r option.", ver, BRIGHTNESS_VER);
    }

    monitor_ids
}

fn check_config_file() -> bool {
    fs::metadata(get_xdg_config()).is_ok()
}

fn set_display_brightness(monitor: Monitor, brightness_value: u8) -> ControlFlow<()> {
    let bus: u8 = monitor.get_monitor_i2c();

    if !bus.eq(&0) {
        let output = Command::new("ddcutil")
            .arg("setvcp")
            .arg("10")
            .arg(format!("{}", brightness_value))
            .arg("--bus")
            .arg(format!("{}", bus))
            .arg("--enable-dynamic-sleep")
            .arg("--noverify")
            .output()
            .expect("Failed to execute ddcutil setvcp");

        if !output.status.success() {
            println!(
                "Error setting brightness for monitor {}: {}",
                monitor.get_monitor_drm(),
                String::from_utf8_lossy(&output.stderr)
            );
            return ControlFlow::Break(());
        }
    }
    ControlFlow::Continue(())
}

fn main() {
    let matches = App::new("Control your monitor(s) brightness")
        .arg(
            Arg::with_name("rebuild")
                .long("rebuild")
                .short("r")
                .help("Rebuild the config file"),
        )
        .arg(
            Arg::with_name("brightness")
                .multiple(true)
                .help("Brightness value")
                .validator(is_valid_integers),
        )
        .get_matches();

    // Loads i2c-dev if it's not loaded
    if !is_i2cdev_loaded() { load_i2cdev(); }

    let monitors = get_monitors();

    // Loads monitor IDs from the config
    let mut monitor_ids: Vec<u8> = read_config_file();

    let mut all_displays_selected: bool = false;

    if matches.is_present("rebuild") || !check_config_file() {
        println!("Rebuilding the config file...");

        if fs::remove_file(get_xdg_config()).is_err() {
            println!("Error removing {}", get_xdg_config().display());
        }

        for (index, monitor) in monitors.iter().enumerate() {
            println!("\nMonitor {}:", index+1);
            monitor.to_string(); // Prints monitor info (see monitor.rs)
        }

        loop {
            println!("\nEnter the monitor number(s), separated by spaces, 0 for all disaplys: ");

            monitor_ids = input_monitor_ids();

            // Check if any inputed IDs are are greater than the number of dispalys
            let mut invalid_input: bool = false;
            for ids in &monitor_ids {
                if *ids as usize > monitors.len() {
                    invalid_input = true;
                }
            }

            // Loop until there are only valid display IDs
            if !invalid_input {
                break;
            }

            println!("Input Failed.");
        }

        // Check if all displays was selected
        if monitor_ids.len() == 1 && monitor_ids[0] == 0 {
            all_displays_selected = true; 
        }
        else {
            for (_index, id) in monitor_ids.iter_mut().enumerate() {
                *id = monitors[(*id as usize) -1].get_monitor_uid();
            }
        }
        
        if write_config_file(&monitor_ids).is_err() {
            println!("\nError writing file. Please try again");
            return;
        } 
    }
    else {
        // Check if all displays was selected
        if monitor_ids.len() == 1 && monitor_ids[0] == 0 { all_displays_selected = true; }
    }

    // If the command included brightness values save them to the vector
    let brightness_values: Vec<u8>;
    brightness_values = if let Some(ints) = matches.values_of("brightness") {
        ints.map(|v| v.parse().unwrap()).collect()
    } 
    // Ask user for values
    else {
        println!("Please enter brightness values (0-100) separated by spaces:");
        get_valid_integers()
    };

    // Create a vector to store the selected monitors
    let mut selected_monitors: Vec<monitor::Monitor> = Vec::new();

    // Add every selected monitor to the list
    if all_displays_selected {
        // Clone the entire monitors vector
        selected_monitors = monitors.clone();
    } 
    else {
        for monitor in monitors {
            if monitor_ids.contains(&(monitor.get_monitor_uid() as u8)) {
                selected_monitors.push(monitor.clone());
            }
        } 
    }

    // Iterate over monitor IDs and brightness values to set brightness
    let mut index = 0;
    for monitor in selected_monitors {
        if let ControlFlow::Break(_) = set_display_brightness(monitor, brightness_values[index]) {
            return;
        }

        if !(index+1 >= brightness_values.len()) {
            index += 1;
        } 
    }
}
